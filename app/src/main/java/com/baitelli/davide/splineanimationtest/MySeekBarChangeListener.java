package com.baitelli.davide.splineanimationtest;

import android.content.Context;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Davide on 23/03/2018.
 */

public class MySeekBarChangeListener implements SeekBar.OnSeekBarChangeListener, ISeekBar {

    private TextView displayValueTView;
    private float seekBarValue;
    private float seekbarMin;
    private float seekbarMax;
    private boolean secondsFormat;

    MySeekBarChangeListener(TextView displayValueTView, float seekbarMin, float seekbarMax, boolean secondsFormat, int startValue) {
        this.displayValueTView = displayValueTView;
        this.seekbarMin = seekbarMin;
        this.seekbarMax = seekbarMax;
        this.secondsFormat = secondsFormat;
        findRealValueAndDisplay(startValue);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        findRealValueAndDisplay(i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private float findRealSeekBarValue(int percentage) {
        return (percentage / 20.0f) * (seekbarMax - seekbarMin) + seekbarMin;
    }

    @Override
    public float getSeekBarValue() {
        return seekBarValue;
    }

    public void findRealValueAndDisplay(int i) {
        seekBarValue = findRealSeekBarValue(i);
        if(secondsFormat)
            displayValueTView.setText(Float.toString(seekBarValue).substring(0, 3) + " s");
        else
            displayValueTView.setText(Float.toString(seekBarValue).substring(0, 3));
    }


    public boolean isSecondsFormat() {
        return secondsFormat;
    }

    public void setSecondsFormat(boolean secondsFormat) {
        this.secondsFormat = secondsFormat;
    }
}
