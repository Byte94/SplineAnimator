package com.baitelli.davide.splineanimationtest;

/**
 * Created by Davide on 21/03/2018.
 */

public class FPoint {
    private float x;

    // Constructor
    public FPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    private float y;


}
