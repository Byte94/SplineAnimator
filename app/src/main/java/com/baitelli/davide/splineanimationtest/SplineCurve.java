package com.baitelli.davide.splineanimationtest;

import java.util.ArrayList;

/**
 * Created by Davide on 27/03/2018.
 */

public class SplineCurve {

    // push the points of the animation in this queue
    private ArrayList<FPoint> pointsQueue = new ArrayList<>();

    // parameters
    private float xStart, yStart;
    private float splineSegmentDuration = 1;
    private float derivativeMultiplier = 1;

    private float x0, y0, x1, y1, dx0, dy0, dx1, dy1, lastDx1, lastDy1;
    private FPoint newPosition;

    // t is higher than 1 at the beginning, so a new spline segment is needed and should be found (see reset())
    private float t;
    private boolean running = false;

    // are there enough point to play an animation?
    private boolean isMoving = false;

    // Constructor
    public SplineCurve(float xStart, float yStart) {
        this.xStart = xStart;
        this.yStart = yStart;
        reset();
    }

    private FPoint splineSegment() {

        float f1, f2, f3, f4;
        float x, y;

        // Blending functions
        f1 = (float) (2 * Math.pow(t, 3) - 3 * Math.pow(t, 2) + 1);
        f2 = (float) (-2 * Math.pow(t, 3) + 3 * Math.pow(t, 2));
        f3 = (float) (Math.pow(t, 3) - 2 * Math.pow(t, 2) + t);
        f4 = (float) (Math.pow(t, 3) - Math.pow(t, 2));

        // Solve equations
        x = f1 * x0 + f2 * x1 + f3 * dx0 + f4 * dx1;
        y = f1 * y0 + f2 * y1 + f3 * dy0 + f4 * dy1;

        return new FPoint(x, y);
    }

    public FPoint getCurrentSplineCoord(float fps) {
        if(running) {
            /* 2 cases: t is in [0, 1] and t has exceed the limit and a new point is needed
            in this second case it's also possible that the sprite stops */
            if (t >= 1) {

                // if there are at least 2 points
                if (pointsQueue.size() >= 2) {

                    // Time to switch to the next point
                    x0 = pointsQueue.get(0).getX();
                    y0 = pointsQueue.get(0).getY();
                    x1 = pointsQueue.get(1).getX();
                    y1 = pointsQueue.get(1).getY();

                    // in the character has stopped (or it has never moved) we start a new spine (new dx0, dy0)
                    if (!isMoving) {
                        // set their derivative to a vector directed to the 2nd point
                        lastDx1 = x1 - x0;
                        lastDy1 = y1 - y0;
                    }

                    dx0 = lastDx1;
                    dy0 = lastDy1;
                    dx1 = (x1 - x0) * derivativeMultiplier;
                    dy1 = (y1 - y0) * derivativeMultiplier;
                    lastDx1 = dx1;
                    lastDy1 = dy1;
                    pointsQueue.remove(0); // removes FPoint in the head and shift

                    // debug print
                    // System.out.println("x0: " + x0 + ", y0: " + y0 + ", x1: " + x1 + ", y1: " + y1);
                    // System.out.println("dx0: " + dx0 + ", dy0: " + dy0 + ", dx1: " + dx1 + ", dy1: " + dy1);

                    t = 0; // reset t

                    isMoving = true;

                } else {

                    // else, do nothing and most important, do not change position
                    isMoving = false;
                }

            } else {
                // t can be further increased
                newPosition = splineSegment();
                t += 1 / (splineSegmentDuration * fps);
            }
        }
        return newPosition;
    }

    public void addPoint(FPoint newPoint) {
        pointsQueue.add(newPoint);
    }

    public void start() {
        running = true;
    }

    public void stop() {
        running = false;
    }

    public void reset() {
        running = false;
        newPosition = new FPoint(xStart, yStart);
        t = 1.5f;
        lastDx1 = 0;
        lastDy1 = 0;
    }

    public float getSplineSegmentDuration() {
        return splineSegmentDuration;
    }

    public void setSplineSegmentDuration(float splineSegmentDuration) {
        this.splineSegmentDuration = splineSegmentDuration;
    }

    public float getDerivativeMultiplier() {
        return derivativeMultiplier;
    }

    public void setDerivativeMultiplier(float derivativeMultiplier) {
        this.derivativeMultiplier = derivativeMultiplier;
    }

    public boolean isMoving() {
        return isMoving;
    }

}
