package com.baitelli.davide.splineanimationtest;

/**
 * Created by Davide on 23/03/2018.
 */

public interface ISeekBar {
    float getSeekBarValue();

}
