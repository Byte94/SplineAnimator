package com.baitelli.davide.splineanimationtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * Created by Davide on 21/03/2018.
 */

// Note how the final closing curly brace }
// is inside SpriteSheetAnimation

// Notice we implement runnable so we have
// A thread and can override the run method.
public class GameView extends SurfaceView implements Runnable {

    // This is our thread
    Thread gameThread = null;

    // This is new. We need a SurfaceHolder
    // When we use Paint and Canvas in a thread
    // We will see it in action in the draw method soon.
    SurfaceHolder ourHolder;

    // A boolean which we will set and unset
    // when the game is running- or not.
    volatile boolean playing;

    // A Canvas and two Paint object
    Canvas canvas;
    Paint fpsPaint;
    Paint wakePaint;
    Paint touchPaint;

    // This variable tracks the game frame rate
    long fps;

    // This is used to help calculate the fps
    private long currentFrameDuration;

    // Declare an object of type Bitmap
    Bitmap bitmapSprite;

    // Sprite position coordinates
    float spriteXPosition = 200;
    float spriteYPosition = 200;

    // Interface to get seekBar value
    ISeekBar speedSeekBar;
    ISeekBar mulSeekbar;

    // Class that animate the sprite following a spline curve
    SplineCurve splineAnimator;

    // Semaphore to protect targetPoints queue
    Semaphore targetPointSem;

    // Last points queue, to draw the path of the character
    ArrayList<FPoint> lastDrawnPoints = new ArrayList<>();
    ArrayList<FPoint> targetPoints = new ArrayList<>();

    // When the we initialize (call new()) on gameView
    // This special constructor method runs
    public GameView(Context context, ISeekBar speedSeekBar, ISeekBar mulSeekbar) {
        // The next line of code asks the
        // SurfaceView class to set up our object.
        // How kind.
        super(context);

        this.speedSeekBar = speedSeekBar;
        this.mulSeekbar = mulSeekbar;

        // Initialize ourHolder and paint objects
        ourHolder = getHolder();
        fpsPaint = new Paint();
        wakePaint = new Paint();
        touchPaint = new Paint();

        fpsPaint.setColor(Color.argb(255,  250, 250, 250));
        fpsPaint.setTextSize(45);
        wakePaint.setColor(Color.argb(255,  63 , 208, 56));
        wakePaint.setStrokeWidth(10);
        wakePaint.setStyle(Paint.Style.FILL);
        touchPaint.setColor(Color.argb(255,  255 , 20, 20));
        touchPaint.setStrokeWidth(10);
        touchPaint.setStyle(Paint.Style.FILL);

        targetPointSem = new Semaphore(1);

        // Load Sprite from his .png file
        bitmapSprite = BitmapFactory.decodeResource(this.getResources(), R.drawable.green_guy);

        splineAnimator = new SplineCurve(spriteXPosition, spriteYPosition);

        // the first point of the spline curve is the initial position of the sprite
        splineAnimator.addPoint(new FPoint(spriteXPosition, spriteYPosition));
        splineAnimator.start();


    }

    @Override
    public void run() {
        while (playing) {

            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            // Update the frame
            update();

            // Draw the frame
            draw();

            // Calculate the fps this frame
            // We can then use the result to
            // time animations and more.
            currentFrameDuration = System.currentTimeMillis() - startFrameTime;
            if (currentFrameDuration >= 1) {
                fps = 1000 / currentFrameDuration;
            }
        }
    }

    // Everything that needs to be updated goes in here
    public void update() {

        // Set parameters to the spline animator
        splineAnimator.setSplineSegmentDuration(speedSeekBar.getSeekBarValue());
        splineAnimator.setDerivativeMultiplier(mulSeekbar.getSeekBarValue());

        // call to the function that calculate new coordinates
        /* t parameter is increased by a value depending on fps and the desidered duration
        of the movement between two points of the spline (fps:1 = tIncrement:splineSegmentDuration) */
        FPoint coord = splineAnimator.getCurrentSplineCoord(fps);

        if(splineAnimator.isMoving())
            lastDrawnPoints.add(coord);
        /* Remove oldest point if there are too much points or if the character is not moving
        and there are still some points in the queue */
        if(lastDrawnPoints.size() > 100 || (lastDrawnPoints.size() > 0 && !splineAnimator.isMoving())) {
            lastDrawnPoints.remove(0);
            // Remove target point if the wake isn't visible
            if(lastDrawnPoints.isEmpty())
                targetPoints.clear();
        }


        spriteXPosition = coord.getX() - bitmapSprite.getWidth() / 2;
        spriteYPosition = coord.getY() - bitmapSprite.getHeight() / 2;

    }

    // Draw the newly updated scene
    public void draw() {

        // Make sure our drawing surface is valid or we crash
        if (ourHolder.getSurface().isValid()) {
            // Lock the canvas ready to draw
            canvas = ourHolder.lockCanvas();

            // Draw the background color
            canvas.drawColor(Color.argb(255,  23, 41, 40));

            // Display the current fps on the screen
            canvas.drawText("FPS:" + fps, 20, 55, fpsPaint);

            // Draw the wake
            for(FPoint p : lastDrawnPoints) {
                canvas.drawCircle(p.getX(), p.getY(), 10, wakePaint);
            }

            targetPointSem.acquireUninterruptibly();
            // Draw points where the user has clicked
            for(FPoint p : targetPoints) {
                canvas.drawCircle(p.getX(), p.getY(), 20, touchPaint);
            }
            targetPointSem.release();

            // Draw the sprite at spriteXPosition, 200 pixels
            canvas.drawBitmap(bitmapSprite, spriteXPosition, spriteYPosition, fpsPaint);

            // Draw everything to the screen
            ourHolder.unlockCanvasAndPost(canvas);
        }

    }

    // If SimpleGameEngine Activity is paused/stopped
    // shutdown our thread.
    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

    }

    // If SimpleGameEngine Activity is started then
    // start our thread.
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    // The SurfaceView class implements onTouchListener
    // So we can override this method and detect screen touches.
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            // Player has touched the screen
            case MotionEvent.ACTION_DOWN:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                splineAnimator.addPoint(new FPoint(x, y));
                try {
                    targetPointSem.acquire();
                    targetPoints.add(new FPoint(x, y));
                    targetPointSem.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e("asd", "MUTEX ERROR");
                }

                break;
        }
        return true;
    }




}
