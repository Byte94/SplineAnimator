package com.baitelli.davide.splineanimationtest;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    GameView gameView;
    public static final float SPEED_SEEKBAR_MIN = 0.1f;
    public static final float SPEED_SEEKBAR_MAX= 2.0f;
    public static final float MUL_SEEKBAR_MIN = 0.5f;
    public static final float MUL_SEEKBAR_MAX = 6f;

    private Toolbar myToolbar;
    final Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SeekBar speedSeekBar = findViewById(R.id.speed_seekbar);
        SeekBar mulSeekBar = findViewById(R.id.mul_seekbar);
        TextView speedSeekBarValueTView = findViewById(R.id.speed_seekbar_value_textview);
        TextView mulSeekBarValueTView = findViewById(R.id.mul_seekbar_value_textview);

        // Create the listeners for the two seekbars
        MySeekBarChangeListener speedSeekBarListener = new MySeekBarChangeListener(speedSeekBarValueTView,
                SPEED_SEEKBAR_MIN, SPEED_SEEKBAR_MAX, true, 10);
        speedSeekBar.setOnSeekBarChangeListener(speedSeekBarListener);

        MySeekBarChangeListener mulSeekBarListener = new MySeekBarChangeListener(mulSeekBarValueTView,
                MUL_SEEKBAR_MIN, MUL_SEEKBAR_MAX, false, 2);
        mulSeekBar.setOnSeekBarChangeListener(mulSeekBarListener);

        // Info button
        assignDialog();

        setToolbar();

        // Initialize gameView and add it to the activity (in a layout)
        gameView = new GameView(this, speedSeekBarListener, mulSeekBarListener);
        LinearLayout container = findViewById(R.id.surfaceview_container);
        container.addView(gameView);



    }

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the gameView resume method to execute
        gameView.resume();

    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the gameView pause method to execute
        gameView.pause();
    }

    public void assignDialog() {

        // Derivate multiplier info button
        ImageButton infoButton = findViewById(R.id.info_image_button);
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog toShow = new MyCustomDialog(mActivity, R.layout.layout_mul_info_dialog, R.id.button_close_mul_info_dialog);
                toShow.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                toShow.show();
            }
        });
    }

    public void setToolbar() {
        // Attaching the layout to the toolbar object
        myToolbar = (Toolbar) findViewById(R.id.tool_bar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(myToolbar);
    }

    // MENU CREATION, on toolbar

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.about_toolbar_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.about_option_menu) {
            Dialog toShow = new MyCustomDialog(mActivity, R.layout.layout_about_dialog, R.id.close_button_about_dialog);
            toShow.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            toShow.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

