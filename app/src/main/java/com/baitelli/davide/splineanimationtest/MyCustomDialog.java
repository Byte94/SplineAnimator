package com.baitelli.davide.splineanimationtest;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class MyCustomDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity activity;
    public Button close;

    private int dialogId;
    private int closeButtonId;

    public MyCustomDialog(Activity activity, int dialogId, int closeButtonId) {
        super(activity);
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.dialogId = dialogId;
        this.closeButtonId = closeButtonId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(dialogId);
        close = (Button) findViewById(closeButtonId);
        close.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
